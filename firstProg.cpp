#include <iostream>
int main(){
  double var = 2.4;
  double* ptr = &var;
  std::cout<<"variable value: "<<var<<"; address: "<<&var<<std::endl;
  std::cout<<"Pointer value: "<<ptr<<" address: "<<&ptr<<"; dereference: "<<*ptr<<std::endl;
}
